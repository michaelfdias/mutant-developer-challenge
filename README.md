# Mutant Developer Challenge

![mutant app print](https://bitbucket.org/michaelfdias/mutant-developer-challenge/downloads/mutant-app-print.png)

## Requirements

- Docker 18
- Docker Compose 1.23.*

## Setup

### Production environment

```sh
make run
```

Access http://localhost:8080


### Development environment

```sh
make dev
```

Access http://localhost:8080


### Test environment

```sh
make test
```
