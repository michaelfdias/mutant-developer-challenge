DEV_FILE = docker-compose.development.yml
TEST_FILE = docker-compose.test.yml

run:
	docker-compose build
	docker-compose up -d

dev:
	docker-compose -f $(DEV_FILE) build
	docker-compose -f $(DEV_FILE) up -d

test:
	docker-compose -f $(TEST_FILE) build
	docker-compose -f $(TEST_FILE) up
