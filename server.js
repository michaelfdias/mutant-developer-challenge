const app = require('./app')

const ENV = process.env.NODE_ENV || 'production'
const PORT = process.env.NODE_PORT || 8080

app.listen(PORT, () => {
  console.log(`[${ENV}] app running on port ${PORT}`)
})
