FROM node:10.16.0-alpine

ENV HOME=/usr/app

COPY package.json package-lock.json $HOME/mutant-developer-challenge/

WORKDIR $HOME/mutant-developer-challenge

RUN npm i && npm cache clean --force

COPY . .

CMD [ "npm", "start" ]
