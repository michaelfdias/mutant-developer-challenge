const express = require('express')

const UserService = require('../models/services/User')

const router = express.Router()

router.get('/', async (req, res) => {
  const addressFilter = /suite/i

  const condition = { 'address.suite': addressFilter }
  const project = ['name', 'email', 'website', 'company.name']
  const sort = { 'company.name': 'asc' }

  const users = await UserService.find(condition, project, sort)

  res.render('home', { users })
})

module.exports = app => app.use(/^\/(home)?$/, router)
