const axios = require('axios')

class User {
  static async find (where = null, project = null, sort = null) {
    let users = await axios.get('https://jsonplaceholder.typicode.com/users')

    users = users.data

    if (where) {
      users = User.where(users, where)
    }

    if (sort) {
      users = User.sort(users, sort)
    }

    if (project) {
      users = User.project(users, project)
    }

    return users
  }

  static resolveHierarchy (path, user) {
    return path.split('.').reduce((previous, current) => {
      return previous ? previous[current] : null
    }, user)
  }

  static createHierarchy (path, lastValue = null) {
    let props = path.split('.')

    let hierarchy
    let lastPropValue = lastValue

    for (let i = (props.length - 1); i >= 0; i--) {
      hierarchy = {}
      hierarchy[props[i]] = lastPropValue
      lastPropValue = hierarchy
    }

    return hierarchy
  }

  static where (users, where) {
    for (let condition in where) {
      users = users.filter(user => {
        return User.resolveHierarchy(condition, user).match(where[condition])
      })
    }

    return users
  }

  static sort (users, sort) {
    const field = Object.keys(sort).shift()
    const direction = sort[field]

    return users.sort(function (a, b) {
      const aValue = User.resolveHierarchy(field, a)
      const bValue = User.resolveHierarchy(field, b)

      if (aValue > bValue) {
        return direction === 'asc' ? 1 : -1
      }

      if (bValue > aValue) {
        return direction === 'desc' ? 1 : -1
      }

      return 0
    })
  }

  static project (users, project) {
    users = users.map(user => {
      const userProject = {}

      project.forEach(projectItem => {
        const projectItemValue = User.resolveHierarchy(projectItem, user)
        const projectItemHierarchy = User.createHierarchy(projectItem, projectItemValue || null)
        Object.assign(userProject, projectItemHierarchy)
      })

      return userProject
    })

    return users
  }
}

module.exports = User
