/* eslint-env jest */

const UserService = require('../../../../src/models/services/User')

const user1 = {
  name: 'Leanne Graham',
  email: 'Sincere@april.biz',
  website: 'hildegard.org',
  company: {
    name: 'Romaguera-Crona',
    catchPhrase: 'Multi-layered client-server neural-net'
  }
}

const user2 = {
  name: 'Ervin Howell',
  email: 'Shanna@melissa.tv',
  website: 'anastasia.net',
  company: {
    name: 'Deckow-Crist',
    catchPhrase: 'Proactive didactic contingency'
  }
}

const user3 = {
  name: 'Clementine Bauch',
  email: 'Nathan@yesenia.net',
  website: 'ramiro.info',
  company: {
    name: 'Romaguera-Jacobson',
    catchPhrase: 'Face to face bifurcated interface'
  }
}

const user4 = {
  name: 'Patricia Lebsack',
  email: 'Julianne.OConner@kory.org',
  website: 'kale.biz',
  company: {
    name: 'Robel-Corkery',
    catchPhrase: 'Multi-tiered zero tolerance productivity'
  }
}

const user5 = {
  name: 'Chelsey Dietrich',
  email: 'Lucio_Hettinger@annie.ca',
  website: 'demarco.info',
  company: {
    name: 'Keebler LLC',
    catchPhrase: 'User-centric fault-tolerant solution'
  }
}

const user6 = {
  name: 'Mrs. Dennis Schulist',
  email: 'Karley_Dach@jasper.info',
  website: 'ola.org',
  company: {
    name: 'Considine-Lockman',
    catchPhrase: 'Synchronised bottom-line interface'
  }
}

const user7 = {
  name: 'Kurtis Weissnat',
  email: 'Telly.Hoeger@billy.biz',
  website: 'elvis.io',
  company: {
    name: 'Johns Group',
    catchPhrase: 'Configurable multimedia task-force'
  }
}

const user8 = {
  name: 'Nicholas Runolfsdottir V',
  email: 'Sherwood@rosamond.me',
  website: 'jacynthe.com',
  company: {
    name: 'Abernathy Group',
    catchPhrase: 'Implemented secondary concept'
  }
}

const user9 = {
  name: 'Glenna Reichert',
  email: 'Chaim_McDermott@dana.io',
  website: 'conrad.com',
  company: {
    name: 'Yost and Sons',
    catchPhrase: 'Switchable contextually-based project'
  }
}

const user10 = {
  name: 'Clementina DuBuque',
  email: 'Rey.Padberg@karina.biz',
  website: 'ambrose.net',
  company: {
    name: 'Hoeger LLC',
    catchPhrase: 'Centralized empowering task-force'
  }
}

const users = [user1, user2, user3, user4, user5, user6, user7, user8, user9, user10]

describe('User', function () {
  describe('where', function () {
    test('Should filter correctly based on properties at the first level', function () {
      expect(UserService.where(users, { email: '.biz' })).toStrictEqual([user1, user7, user10])
      expect(UserService.where(users, { website: '.info' })).toStrictEqual([user3, user5])
    })

    test('Should filter correctly based on nested properties', function () {
      expect(UserService.where(users, { 'company.name': 'LLC' })).toStrictEqual([user5, user10])
      expect(UserService.where(users, { 'company.name': 'Group' })).toStrictEqual([user7, user8])
    })

    test('Should filter correctly with more than one condition', function () {
      expect(UserService.where(users, { email: '.biz', website: '.org' })).toStrictEqual([user1])
      expect(UserService.where(users, { website: '.info', 'company.name': 'LLC' })).toStrictEqual([user5])
    })

    test('Should filter correctly with a regex condition', function () {
      expect(UserService.where(users, { email: /\.[a-z]{2}$/i })).toStrictEqual([user2, user5, user8, user9])
    })
  })

  describe('sort', function () {
    const usersToSort = [user1, user2, user3, user4, user5]

    test('Should sort correctly based on a property at first level', function () {
      expect(UserService.sort(usersToSort, { name: 'asc' })).toStrictEqual([user5, user3, user2, user1, user4])
      expect(UserService.sort(usersToSort, { name: 'desc' })).toStrictEqual([user4, user1, user2, user3, user5])
    })

    test('Should sort correctly based on a nested property', function () {
      expect(UserService.sort(usersToSort, { 'company.name': 'asc' })).toStrictEqual([user2, user5, user4, user1, user3])
      expect(UserService.sort(usersToSort, { 'company.name': 'desc' })).toStrictEqual([user3, user1, user4, user5, user2])
    })
  })

  describe('project', function () {
    test('Should project correctly based on properties at first level', function () {
      expect(UserService.project([user1, user2], ['name', 'email'])).toStrictEqual([
        { name: user1.name, email: user1.email },
        { name: user2.name, email: user2.email }
      ])
    })

    test('Should project correctly based on nested properties', function () {
      expect(UserService.project([user1, user2], ['name', 'company.name'])).toStrictEqual([
        { name: user1.name, company: { name: user1.company.name } },
        { name: user2.name, company: { name: user2.company.name } }
      ])
    })
  })

  describe('createHierarchy', function () {
    test('Should create hierarchy correctly with the default value', function () {
      expect(UserService.createHierarchy('a')).toStrictEqual({ a: null })
      expect(UserService.createHierarchy('a.b')).toStrictEqual({ a: { b: null } })
      expect(UserService.createHierarchy('a.b.c')).toStrictEqual({ a: { b: { c: null } } })
      expect(UserService.createHierarchy('a.b.c.d')).toStrictEqual({ a: { b: { c: { d: null } } } })
    })

    test('Should create hierarchy correctly with a defined value', function () {
      expect(UserService.createHierarchy('a', {})).toStrictEqual({ a: {} })
      expect(UserService.createHierarchy('a.b', 1)).toStrictEqual({ a: { b: 1 } })
      expect(UserService.createHierarchy('a.b.c', true)).toStrictEqual({ a: { b: { c: true } } })
      expect(UserService.createHierarchy('a.b.c.d', 'acme')).toStrictEqual({ a: { b: { c: { d: 'acme' } } } })
    })
  })

  describe('resolveHierarchy', function () {
    test('Should resolve hierarchy correctly based on properties at first level', function () {
      expect(UserService.resolveHierarchy('name', user1)).toBe(user1.name)
      expect(UserService.resolveHierarchy('email', user1)).toBe(user1.email)
      expect(UserService.resolveHierarchy('website', user1)).toBe(user1.website)
    })

    test('Should resolve hierarchy correctly base on nested properties', function () {
      expect(UserService.resolveHierarchy('company.name', user1)).toBe(user1.company.name)
      expect(UserService.resolveHierarchy('company.catchPhrase', user1)).toBe(user1.company.catchPhrase)
    })
  })
})
